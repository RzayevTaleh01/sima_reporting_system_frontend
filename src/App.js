import TableComponent from "./components/shared/TableComponent";
import db from "./db.json";

function App() {
  return (
    <div className="App">
      <TableComponent data={db.comments} withLink={true}></TableComponent>
    </div>
  );
}

export default App;
